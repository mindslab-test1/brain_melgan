import os
import glob
import time
import torch
import random
import librosa
import numpy as np
from torch.utils.data import Dataset, DataLoader

from utils.utils import read_wav_np


def create_dataloader(hp, args, train):
    dataset = AudioDataset(hp, args, train)

    if train:
        return DataLoader(dataset=dataset, batch_size=hp.train.batch_size, shuffle=True,
            num_workers=hp.train.num_workers, pin_memory=True, drop_last=True)
    else:
        return DataLoader(dataset=dataset, batch_size=1, shuffle=False,
            num_workers=hp.train.num_workers, pin_memory=True, drop_last=False)


class AudioDataset(Dataset):
    def __init__(self, hp, args, train):
        self.hp = hp
        self.args = args
        self.train = train
        self.cnt = 0

        if train:
            self.wav_list = list()
            for path in hp.data.train:
                temp = glob.glob(os.path.join(path, '**', '*.wav'), recursive=True)
                self.wav_list.append(temp)
                self.cnt += len(temp)
        else:
            self.wav_list = glob.glob(os.path.join(hp.data.validation, '**', '*.wav'), recursive=True)
            self.cnt += len(self.wav_list)

    def __len__(self):
        return self.cnt

    def __getitem__(self, idx):
        if self.train:
            return self.my_getitem(idx), self.my_getitem(idx+1)
        else:
            return self.my_getitem(idx)

    def balanced_sampling(self, idx):
        if self.train:
            random.seed(time.time() + idx)
            temp = random.choice(self.wav_list)
            return random.choice(temp)
        else:
            return self.wav_list[idx]

    def my_getitem(self, idx):
        wavpath = self.balanced_sampling(idx)
        sr, audio = read_wav_np(wavpath)
        assert sr == self.hp.audio.sampling_rate, 'sr mismatch at %s' % wavpath

        if self.train:
            audio = self.match_audio_length(audio)
            audio /= max(np.max(np.abs(audio)), 1e-2)
            audio *= random.uniform(0.3, 0.99)

            if random.random() < 0.5:
                audio *= -1
        else:
            if audio.size % self.hp.audio.hop_length != 0:
                audio = audio[:-(audio.size % self.hp.audio.hop_length)]


        audio = torch.from_numpy(audio).unsqueeze(0)
        audio = audio + (1/32768) * torch.randn_like(audio)

        return audio

    def match_audio_length(self, audio):
        if len(audio) < self.hp.audio.segment_length:
            temp = random.randint(0, self.hp.audio.segment_length - len(audio))
            audio = np.pad(audio, (temp, self.hp.audio.segment_length - len(audio) - temp), \
                        mode='constant', constant_values=0.0)
        else:
            temp = random.randint(0, len(audio) - self.hp.audio.segment_length)
            audio = audio[temp:temp+self.hp.audio.segment_length]

        return audio
