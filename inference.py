import os
import glob
import tqdm
import torch
import argparse
from scipy.io.wavfile import write
from omegaconf import OmegaConf

from model.generator import Generator
from datasets.mel import Audio2Mel
from utils.utils import read_wav_np

MAX_WAV_VALUE = 32768.0


def main(args):
    checkpoint = torch.load(args.checkpoint_path)
    hp = OmegaConf.load(args.config)

    model = Generator(hp.audio.n_mel_channels).cuda()
    model.load_state_dict(checkpoint['model_g'])
    model.eval(inference=False)
    wav2mel = Audio2Mel(
        n_fft=hp.audio.filter_length,
        hop_length=hp.audio.hop_length,
        win_length=hp.audio.win_length,
        sampling_rate=hp.audio.sampling_rate,
        n_mel_channels=hp.audio.n_mel_channels,
        mel_fmin=hp.audio.mel_fmin,
        mel_fmax=hp.audio.mel_fmax,
    ).cuda()

    if args.type == 'wav':
        with torch.no_grad():
            for wavpath in tqdm.tqdm(glob.glob(os.path.join(args.input_folder, '**', '*.wav'), recursive=True)):
                sr, audio = read_wav_np(wavpath)
                assert sr == hp.audio.sampling_rate, 'sr mismatch: %s' % wavpath
                audio = torch.from_numpy(audio).view(1, 1, -1)
                audio = audio.cuda()
                mel = wav2mel(audio)

                reconstruction = model.inference(mel)
                reconstruction = reconstruction.cpu().detach().numpy()

                out_path = wavpath.replace('.wav', '_reconstructed_epoch%04d.wav' % checkpoint['epoch'])
                write(out_path, hp.audio.sampling_rate, reconstruction)
    elif args.type == 'mel':
        with torch.no_grad():
            for melpath in tqdm.tqdm(glob.glob(os.path.join(args.input_folder, '**', '*.mel'), recursive=True)):
                mel = torch.load(melpath)

                reconstruction = model.inference(mel)
                reconstruction = reconstruction.cpu().detach().numpy()

                out_path = melpath.replace('.mel', '.wav')
                write(out_path, hp.audio.sampling_rate, reconstruction)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for config.")
    parser.add_argument('-p', '--checkpoint_path', type=str, required=True,
                        help="path of checkpoint pt file for evaluation")
    parser.add_argument('-i', '--input_folder', type=str, required=True,
                        help="directory of audios for testing")
    parser.add_argument('-t', '--type', choices=['wav', 'mel'],
                        help="input type")
    args = parser.parse_args()

    main(args)
