import logging
import time
import torch
import grpc
import argparse

from concurrent import futures
from omegaconf import OmegaConf


from model.generator import Generator

import tts_pb2
from tts_pb2_grpc import VocoderServicer
from tts_pb2_grpc import add_VocoderServicer_to_server

MAX_WAV_VALUE = 32768.0
_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class MelganServicerImpl(VocoderServicer):
    def __init__(self, checkpoint_path, device, volume, mel_chunk_size, crossfade_size, config_path):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device
        self.volume = volume
        self.mel_chunk_size = mel_chunk_size
        self.min_wav_value = -MAX_WAV_VALUE
        self.max_wav_value = MAX_WAV_VALUE - 1

        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        hp = OmegaConf.load(config_path)
        self.hp = hp

        self.model = Generator(hp.audio.n_mel_channels).to(device)
        self.model.load_state_dict(checkpoint['model_g'])
        self.model.eval()

        del checkpoint 
        torch.cuda.empty_cache()

        # cross-fade
        self.crossfade_size = crossfade_size
        self.left_filter = torch.linspace(start=1.0, end=0.0, steps=crossfade_size).to(device)
        self.right_filter = torch.linspace(start=0.0, end=1.0, steps=crossfade_size).to(device)

    @torch.no_grad()
    def Mel2Wav(self, in_mel, context):
        torch.cuda.set_device(self.device)
        try:
            mel_total = torch.FloatTensor([d for d in in_mel.data])
            mel_total = mel_total.view(1, self.hp.audio.n_mel_channels, -1)

            previous_audio = None
            for start_idx in range(0, mel_total.size(-1), self.mel_chunk_size):
                mel = mel_total[:, :, start_idx:start_idx+self.mel_chunk_size]
                audio, previous_audio = self._mel2wav(mel, previous_audio)
                yield tts_pb2.WavData(data=audio)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @torch.no_grad()
    def StreamMel2Wav(self, in_mel_iterator, context):
        torch.cuda.set_device(self.device)
        try:
            previous_audio = None
            for in_mel in in_mel_iterator:
                mel = torch.FloatTensor([d for d in in_mel.data])
                mel = mel.view(1, self.hp.audio.n_mel_channels, -1)

                audio, previous_audio = self._mel2wav(mel, previous_audio)
                yield tts_pb2.WavData(data=audio)

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    def _mel2wav(self, mel, previous_audio):
        mel = mel.to(self.device)
        if mel.size(-1) < 4:
            # minimum value of mel from datasets/mel.py is -5.0.
            # minimal time frame for generator is 3 - size of nn.ReflectionPad1d(3).
            pad_size = 4 - mel.size(-1)
            mel = torch.nn.functional.pad(mel, (0, pad_size), 'constant', value=-5.0)

        audio = self.model(mel)
        audio = MAX_WAV_VALUE * audio.squeeze() * self.volume

        # cross-fade. this shortens the audio about crossfade_size per chunk.
        if previous_audio is not None:
            audio[:self.crossfade_size] = \
                previous_audio * self.left_filter + \
                audio[:self.crossfade_size] * self.right_filter
        previous_audio = audio[-self.crossfade_size:]
        audio = audio[:-self.crossfade_size]

        audio = torch.clamp(audio, min=self.min_wav_value, max=self.max_wav_value)
        audio = audio.short().tolist()
        return audio, previous_audio

    def GetMelConfig(self, empty, context):
        mel_config = tts_pb2.MelConfig(
            filter_length=self.hp.audio.filter_length,
            hop_length=self.hp.audio.hop_length,
            win_length=self.hp.audio.win_length,
            n_mel_channels=self.hp.audio.n_mel_channels,
            sampling_rate=self.hp.audio.sampling_rate,
            mel_fmin=self.hp.audio.mel_fmin,
            mel_fmax=self.hp.audio.mel_fmax
        )
        return mel_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='melgan runner executor')
    parser.add_argument('-c', '--config', required=True, type=str,
                        help='yaml file for configuration')
    parser.add_argument('-m', '--model', required=True, type=str,
                        help='Model Path.')
    parser.add_argument('-l', '--log_level', default='INFO', type=str,
                        help='logger level')
    parser.add_argument('-p', '--port', default=35002, type=int,
                        help='grpc port')
    parser.add_argument('-d', '--device', default=0, type=int,
                        help='gpu device')
    parser.add_argument('-v', '--volume', default=1.0, type=float,
                        help='audio volume')
    parser.add_argument('-w', '--max_workers', default=1, type=int,
                        help='max workers')
    parser.add_argument('--crossfade_size', default=200, type=int,
                        help='window size for cross-fade')
    parser.add_argument('--mel_chunk_size', default=88, type=int,
                        help='mel chunk size for Mel2Wav rpc')

    args = parser.parse_args()

    melgan = MelganServicerImpl(
        args.model, args.device, args.volume, args.mel_chunk_size, args.crossfade_size, args.config)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_VocoderServicer_to_server(melgan, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('melgan starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
