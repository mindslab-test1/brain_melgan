FROM nvcr.io/nvidia/pytorch:19.06-py3
RUN python3 -m pip --no-cache-dir install --upgrade \
        omegaconf==2.0.0 \
        gpustat==0.6.0 \
        grpcio==1.13.0 \
        grpcio-tools==1.13.0 \
        protobuf==3.5.1 \
        && \
apt update && \
apt install -y \
    tmux \
    htop \
    ncdu && \
apt clean && \
apt autoremove && \
rm -rf /var/lib/apt/lists/* /tmp/* && \
mkdir /root/melgan
COPY . /root/melgan
RUN cd /root/melgan/ && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. tts.proto
