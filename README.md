# melgan vocoder

- `brain_idl` version: 1.0.0
- mel-spectrogram: neurips mel. 자세한 내용은 https://pms.maum.ai/confluence/x/o0iqAQ 참조
- 관련 컨플루언스 글: https://pms.maum.ai/confluence/x/YmKqAQ

Implementation of https://arxiv.org/abs/1910.06711, based on https://github.com/seungwonpark/melgan.
- Input: mel-spectrogram (see `Audio2Mel` module in `datasets/mel.py`)
- Output: raw audio

## Build

```bash
docker build -f Dockerfile -t melgan:<version> .
docker build -f Dockerfile-server --build-arg melgan_version=<version> -t melgan:<version>-server .
```

## Inference (Serving)

```bash
docker run -d \
--gpus device=0 \
-v <checkpoint_directory>:/model \
-p <port>:35002 \
-e MELGAN_YAML=/model/<path_to_config_yaml> \
-e MELGAN_MODEL=/model/<path_to_checkpoint> \
--name <container_name> \
melgan:<version>
```

## Training

`python trainer.py`. Refer to https://github.com/seungwonpark/melgan for detailed description.

## Author

브레인 박승원 수석
